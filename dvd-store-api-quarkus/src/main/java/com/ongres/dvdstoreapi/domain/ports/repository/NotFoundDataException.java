package com.ongres.dvdstoreapi.domain.ports.repository;

public class NotFoundDataException extends RepositoryException {

  private static final long serialVersionUID = 1L;

  public NotFoundDataException(String message) {
    super(message);
  }

  public NotFoundDataException(Throwable cause) {
    super(cause);
  }

}
