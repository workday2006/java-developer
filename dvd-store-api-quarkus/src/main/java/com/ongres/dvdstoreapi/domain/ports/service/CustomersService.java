package com.ongres.dvdstoreapi.domain.ports.service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.ongres.dvdstoreapi.domain.model.Count;
import com.ongres.dvdstoreapi.domain.ports.repository.CustomersRepository;

public class CustomersService {

  private final CustomersRepository customersRepository;

  public CustomersService(CustomersRepository customersRepository) {
    this.customersRepository = customersRepository;
  }

  public @Nonnull Count countByCountryOrCountryAndCity(
      @Nonnull String country,
      @Nullable String city) {
    if (city == null) {
      return customersRepository.countByCountry(country);
    } else {
      return customersRepository.countByCountryAndCity(country, city);
    }
  }

}
