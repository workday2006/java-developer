package com.ongres.dvdstoreapi.domain.model;

import org.immutables.value.Value;

@Value.Immutable
public interface Count {

  int getCount();

  static ImmutableCount.Builder builder() {
    return ImmutableCount.builder();
  }

}
