package com.ongres.dvdstoreapi.domain.ports.repository;

import java.util.stream.Stream;

import javax.annotation.Nonnull;

import com.ongres.dvdstoreapi.domain.model.RentalOverdue;

public interface RentalRepository {

  @Nonnull Stream<RentalOverdue> rentalOverdues();

}
