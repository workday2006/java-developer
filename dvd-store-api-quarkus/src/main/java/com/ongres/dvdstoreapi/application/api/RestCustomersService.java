package com.ongres.dvdstoreapi.application.api;

import javax.inject.Inject;

import com.ongres.dvdstoreapi.domain.ports.repository.CustomersRepository;
import com.ongres.dvdstoreapi.domain.ports.service.CustomersService;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.ReactiveRoutes;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.Route.HttpMethod;
import io.quarkus.vertx.web.RouteBase;

@RouteBase(path = "/customers")
public class RestCustomersService {

  private final CustomersService customersService;

  @Inject
  public RestCustomersService(CustomersRepository customersRepository) {
    this.customersService = new CustomersService(customersRepository);
  }

  @Route(methods = HttpMethod.GET, path = "/count-by-country/:country",
    produces = ReactiveRoutes.APPLICATION_JSON, type = Route.HandlerType.BLOCKING)
  public int count(
      @Param("country") String country,
      @Param("city") String city
      ) throws Exception {
    return customersService.countByCountryOrCountryAndCity(country, city)
        .getCount();
  }

}