package com.ongres.dvdstoreapi.application.api;

import javax.inject.Inject;

import com.ongres.dvdstoreapi.domain.model.RentalOverdue;
import com.ongres.dvdstoreapi.domain.ports.repository.RentalRepository;
import com.ongres.dvdstoreapi.domain.ports.service.RentalService;
import io.quarkus.vertx.web.ReactiveRoutes;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.Route.HttpMethod;
import io.quarkus.vertx.web.RouteBase;
import io.smallrye.mutiny.Multi;

@RouteBase(path = "/rental")
public class RestRentalService {

  private final RentalService rentalService;

  @Inject
  public RestRentalService(
      RentalRepository rentalRepository) {
    this.rentalService = new RentalService(rentalRepository);
  }

  @Route(methods = HttpMethod.GET, path = "/overdues",
    produces = {
        ReactiveRoutes.APPLICATION_JSON,
        ReactiveRoutes.ND_JSON
    })
  public Multi<RentalOverdue> rentalByActor() {
    return Multi.createFrom()
        .items(rentalService.rentalOverdues());
  }

}