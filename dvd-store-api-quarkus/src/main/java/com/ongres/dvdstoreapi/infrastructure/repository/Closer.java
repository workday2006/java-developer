package com.ongres.dvdstoreapi.infrastructure.repository;

import java.util.ArrayList;
import java.util.List;

class Closer implements AutoCloseable {
  List<AutoCloseable> closeables = new ArrayList<>();

  <T extends AutoCloseable> T append(T closeable) {
    closeables.add(0, closeable);
    return closeable;
  }

  @Override
  public void close() throws Exception {
    Exception exception = null;
    for (AutoCloseable closeable : closeables) {
      try {
        closeable.close();
      } catch (Exception ex) {
        if (exception == null) {
          exception = ex;
        } else {
          exception.addSuppressed(ex);
        }
      }
    }
    if (exception != null) {
      throw exception;
    }
  }

  public <T extends Exception> T close(T exception) {
    try {
      close();
    } catch (Exception ex) {
      exception.addSuppressed(ex);
    }
    return exception;
  }
  
}
