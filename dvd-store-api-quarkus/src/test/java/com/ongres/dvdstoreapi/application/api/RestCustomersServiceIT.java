package com.ongres.dvdstoreapi.application.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import com.ongres.dvdstoreapi.application.api.AbstractRestServiceIT.Sakila;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(Sakila.class)
public class RestCustomersServiceIT extends AbstractRestServiceIT {

  @Test
  public void testCustomersCountWithoutCountry() {
    given()
        .when().get("/customers/count-by-country")
        .then()
        .statusCode(404);
  }

  @Test
  public void testCustomersCountForSpain() {
    given()
        .when().get("/customers/count-by-country/Spain")
        .then()
        .statusCode(200)
        .body(is("5"));
  }

  @Test
  public void testCustomersCountForSalamanca() {
    given()
        .when().get("/customers/count-by-country/Spain?city=Santiago de Compostela")
        .then()
        .statusCode(200)
        .body(is("1"));
  }

  @Test
  public void testCustomersCountForNotFound() {
    given()
        .when().get("/customers/count-by-country/NotFound")
        .then()
        .statusCode(404);
  }

}