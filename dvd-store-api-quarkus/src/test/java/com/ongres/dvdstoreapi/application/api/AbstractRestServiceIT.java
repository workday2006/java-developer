package com.ongres.dvdstoreapi.application.api;

import java.util.Collections;
import java.util.Map;

import io.quarkus.test.junit.QuarkusTestProfile;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public abstract class AbstractRestServiceIT {

  static GenericContainer<?> postgres;

  public static class Sakila implements QuarkusTestProfile {
    @Override
    @SuppressWarnings("resource")
    public Map<String, String> getConfigOverrides() {
      postgres = new GenericContainer<>(DockerImageName.parse(
          "registry.gitlab.com/ongresinc/interview/java-developer/sakila"))
          .withExposedPorts(5432);
      postgres.start();
      Runtime.getRuntime().addShutdownHook(new Thread(postgres::stop));
      postgres.waitingFor(new HostPortWaitStrategy());
      return Collections.singletonMap(
          "quarkus.datasource.jdbc.url",
          "jdbc:postgresql://" + postgres.getHost() + ":" + postgres.getMappedPort(5432) + "/sakila");
    }
  }

}