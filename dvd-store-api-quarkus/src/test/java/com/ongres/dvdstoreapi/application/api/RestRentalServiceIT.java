package com.ongres.dvdstoreapi.application.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import com.ongres.dvdstoreapi.application.api.AbstractRestServiceIT.Sakila;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(Sakila.class)
public class RestRentalServiceIT extends AbstractRestServiceIT {

  @Test
  public void testOverdues() {
    given()
        .when().get("/rental/overdues")
        .then()
        .statusCode(200)
        .assertThat()
        .body("size()", is(183));
  }

}