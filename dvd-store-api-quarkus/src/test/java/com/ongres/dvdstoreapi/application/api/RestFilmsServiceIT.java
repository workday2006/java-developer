package com.ongres.dvdstoreapi.application.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import com.ongres.dvdstoreapi.application.api.AbstractRestServiceIT.Sakila;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(Sakila.class)
public class RestFilmsServiceIT extends AbstractRestServiceIT {

  @Test
  public void testFilmsByActorWithoutActor() {
    given()
        .when().get("/films/find-by-actor")
        .then()
        .statusCode(404);
  }

  @Test
  public void testFilmsByActorForDanTorn() throws Exception {
    given()
        .when().get("/films/find-by-actor/Dan Torn")
        .then()
        .statusCode(200)
        .assertThat()
        .body("size()", is(22));
  }

  @Test
  public void testFilmsByActorForDanTornAndClassicsCategory() {
    given()
        .when().get("/films/find-by-actor/Dan Torn?category=Classics")
        .then()
        .statusCode(200)
        .body("size()", is(3));
  }

  @Test
  public void testFilmsByActorForActorNotFound() {
    given()
        .when().get("/films/find-by-actor/Not Found")
        .then()
        .statusCode(404);
  }

}