# DVD Store Database Project

The DVD Store API project uses [Sakila database port for PostgreSQL by Lukas Eder](https://github.com/jOOQ/sakila/tree/main/postgres-sakila-db) (to see a visual representation of the schema see [this generated schema from SchemaSpy tool](https://schemaspy.sourceforge.net/sakila/).

## Build database Docker image

You can generate a Docker container image that includes the database using the command:

```
docker build -t registry.gitlab.com/ongresinc/interview/java-developer/sakila .
```

## Use database Docker image

Run the image:

```
docker run -ti --rm  --name dvd-store-database registry.gitlab.com/ongresinc/interview/java-developer/sakila
```

Connect to postgres:

```
docker exec -ti dvd-store-database -- psql
```
