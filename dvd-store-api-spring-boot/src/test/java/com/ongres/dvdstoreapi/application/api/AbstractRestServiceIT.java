package com.ongres.dvdstoreapi.application.api;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public abstract class AbstractRestServiceIT {

  static GenericContainer<?> postgres;

  @DynamicPropertySource
  @SuppressWarnings("resource")
  static void registerPostgresProperties(DynamicPropertyRegistry registry) {
    postgres = new GenericContainer<>(DockerImageName.parse(
        "registry.gitlab.com/ongresinc/interview/java-developer/sakila"))
        .withExposedPorts(5432);
    postgres.start();
    Runtime.getRuntime().addShutdownHook(new Thread(postgres::stop));
    postgres.waitingFor(new HostPortWaitStrategy());
    registry.add(
        "spring.datasource.url",
        () -> "jdbc:postgresql://" + postgres.getHost() + ":" + postgres.getMappedPort(5432) + "/sakila");
  }

}