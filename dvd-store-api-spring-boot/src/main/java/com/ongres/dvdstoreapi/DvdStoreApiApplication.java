package com.ongres.dvdstoreapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvdStoreApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvdStoreApiApplication.class, args);
	}

}
