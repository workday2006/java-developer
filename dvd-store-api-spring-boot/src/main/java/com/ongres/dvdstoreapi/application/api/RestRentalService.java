package com.ongres.dvdstoreapi.application.api;

import com.ongres.dvdstoreapi.domain.model.RentalOverdue;
import com.ongres.dvdstoreapi.domain.ports.repository.RentalRepository;
import com.ongres.dvdstoreapi.domain.ports.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/rental")
public class RestRentalService {

  private final RentalService rentalService;

  @Autowired
  public RestRentalService(
      RentalRepository rentalRepository) {
    this.rentalService = new RentalService(rentalRepository);
  }

  @GetMapping(value = "/overdues",
      produces = {
          MediaType.APPLICATION_JSON_VALUE,
          MediaType.APPLICATION_NDJSON_VALUE
      })
  public Flux<RentalOverdue> rentalByActor() {
    return Flux.fromStream(rentalService.rentalOverdues());
  }

}