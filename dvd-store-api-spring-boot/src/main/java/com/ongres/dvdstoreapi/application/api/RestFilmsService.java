package com.ongres.dvdstoreapi.application.api;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;
import com.ongres.dvdstoreapi.domain.ports.repository.FilmsRepository;
import com.ongres.dvdstoreapi.domain.ports.service.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/films")
public class RestFilmsService {

  private final FilmsService filmsService;

  @Autowired
  public RestFilmsService(
      FilmsRepository filmsRepository) {
    this.filmsService = new FilmsService(filmsRepository);
  }

  @GetMapping(value = "/find-by-actor/{actor}",
      produces = {
          MediaType.APPLICATION_JSON_VALUE,
          MediaType.APPLICATION_NDJSON_VALUE
      })
  public Flux<ActorFilm> filmsByActor(
      @PathVariable("actor") String actor,
      @RequestParam(value = "category", required = false) String category
      ) {
    return Flux.fromStream(filmsService.filmByActorAndCategory(actor, category));
  }

}