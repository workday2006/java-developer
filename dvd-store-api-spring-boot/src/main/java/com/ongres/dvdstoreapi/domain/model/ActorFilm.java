package com.ongres.dvdstoreapi.domain.model;

import org.immutables.value.Value;

@Value.Immutable
public interface ActorFilm {

  String getActorFirstName();

  String getActorLastName();
 
  String getFilmTitle();

  String getFilmDescription();

  String getFilmCategory();

  static ImmutableActorFilm.Builder builder() {
    return ImmutableActorFilm.builder();
  }

}
