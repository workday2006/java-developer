package com.ongres.dvdstoreapi.domain.model;

import org.immutables.value.Value;

@Value.Immutable
public interface RentalOverdue {

  String getCustomer();

  String getPhone();
 
  String getTitle();

  static ImmutableRentalOverdue.Builder builder() {
    return ImmutableRentalOverdue.builder();
  }

}
