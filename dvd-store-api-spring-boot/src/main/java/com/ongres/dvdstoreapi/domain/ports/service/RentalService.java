package com.ongres.dvdstoreapi.domain.ports.service;

import java.util.stream.Stream;

import javax.annotation.Nonnull;

import com.ongres.dvdstoreapi.domain.model.RentalOverdue;
import com.ongres.dvdstoreapi.domain.ports.repository.RentalRepository;

public class RentalService {

  private final RentalRepository rentalRepository;

  public RentalService(RentalRepository rentalRepository) {
    this.rentalRepository = rentalRepository;
  }

  public @Nonnull Stream<RentalOverdue> rentalOverdues() {
    return rentalRepository.rentalOverdues();
  }

}
